using Microsoft.AspNetCore.Mvc;
using Takenet.Challenge.Services;

namespace Takenet.Challenge.Controllers;

[ApiController]
[Route("repositories")]
public class RepositoriesController : ControllerBase
{
    private readonly IGitHubService _gitHubService;

    public RepositoriesController(IGitHubService gitHubService)
    {
        _gitHubService = gitHubService;
    }

    [HttpGet("{organization}")]
    public async Task<IActionResult> Get(string organization, string? type = "all", string? sort = "created",
        string? direction = "desc", int? count = 30, int? page = 1)
    {
        var repositories = await _gitHubService.GetRepositories(organization, type, sort, direction, count, page);

        return Ok(repositories);
    }
}
