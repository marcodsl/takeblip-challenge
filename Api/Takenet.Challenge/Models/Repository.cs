namespace Takenet.Challenge.Models;

public class Repository
{
    public string FullName { get; set; }

    public string Description { get; set; }

    public User Owner { get; set; }

    public Repository(string fullName, string description, User owner)
    {
        FullName = fullName;
        Description = description;
        Owner = owner;
    }
}
