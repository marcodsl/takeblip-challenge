namespace Takenet.Challenge.Models;

public class User
{
    public string Login { get; set; }

    public string AvatarUrl { get; set; }

    public User(string login, string avatarUrl)
    {
        Login = login;
        AvatarUrl = avatarUrl;
    }
}
