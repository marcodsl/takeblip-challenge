using Newtonsoft.Json;
using Refit;
using Takenet.Challenge.Services;
using Takenet.Challenge.Utils;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

builder.Services
    .AddRefitClient<IGitHubService>(new RefitSettings
    {
        ContentSerializer = new NewtonsoftJsonContentSerializer(new JsonSerializerSettings
        {
            ContractResolver = new SnakeCasePropertyNamesContractResolver()
        })
    })
    .ConfigureHttpClient(client => client.BaseAddress = new Uri("https://api.github.com"));

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
