using Refit;
using Takenet.Challenge.Models;

namespace Takenet.Challenge.Services;

[Headers("User-Agent: Refit")]
public interface IGitHubService
{
    [Get("/orgs/{org}/repos")]
    Task<Repository[]> GetRepositories([AliasAs("org")] string organization, string? type, string? sort,
        string? direction, [AliasAs("per_page")] int? count, int? page);
}
