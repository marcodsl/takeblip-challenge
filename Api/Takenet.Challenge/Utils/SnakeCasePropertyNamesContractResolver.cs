using System.Text.RegularExpressions;
using Newtonsoft.Json.Serialization;

namespace Takenet.Challenge.Utils;

public class SnakeCasePropertyNamesContractResolver : DefaultContractResolver
{
    protected override string ResolvePropertyName(string propertyName)
    {
        var underscores = Regex.Match(propertyName, "^(_+)");
        var match = Regex
            .Replace(propertyName, @"([A-Z]|([0-9]+))", "_$1")
            .TrimStart('_')
            .ToLower();

        return underscores.Value + match;
    }
}
