# takeblip-challenge

Desafio técnico para a vaga de Pessoa Desenvolvedora da Take Blip

## API

#### Tecnologias utilizadas:

- [ASP.NET Core 6](https://docs.microsoft.com/pt-br/aspnet/core/?view=aspnetcore-6.0) como framework web
- [Refit](https://github.com/reactiveui/refit) para consumir a API do GitHub
  > Escolhi o Refit pela praticidade em representar uma API REST através de código, delegando a responsabilidade de lidar com chamadas HTTP a uma biblioteca confiável e com boa integração ao ASP.NET.
- [Swashbuckle.AspNetCore](https://github.com/domaindrivendev/Swashbuckle.AspNetCore) para documentação em desenvolvimento
- [Docker](https://www.docker.com/) para conteinerização
- [Heroku](heroku.com) para hospedagem
- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) para entrega contínua
